package database

import(
	"fitnessclassbookings/data/datamodels"
)


var classDatabase = make(map[int]datamodels.Class)
var bookingDatabase = make(map[int]datamodels.Booking)

// classes database

func FindAllClasses() []datamodels.Class {
	classes := make([]datamodels.Class, 0, len(classDatabase))
	for _, class := range classDatabase {
		classes = append(classes, class)
	}

	return classes
}

// TODO ROC: Use a database so key can be a uuid
func FindClassById(key int) (datamodels.Class, bool) {
	com, ok := classDatabase[key]
	
	return com, ok
}

func SaveClass(key int, item datamodels.Class) {
	classDatabase[key] = item
}

func UpdateClass(key int, item datamodels.Class) {
	classDatabase[key] = item
}

// booking database


func FindAllBookings() []datamodels.Booking {
	bookings := make([]datamodels.Booking, 0, len(bookingDatabase))
	for _, booking := range bookingDatabase {
		bookings = append(bookings, booking)
	}

	return bookings
}

func FindBookingById(key int) (datamodels.Booking, bool) {
	booking, ok := bookingDatabase[key]
	
	return booking, ok
}

func SaveBooking(key int, item datamodels.Booking) {
	bookingDatabase[key] = item
}

func UpdateBooking(key int, item datamodels.Booking) {
	bookingDatabase[key] = item
}



