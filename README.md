# RESTful API created with golang
This is a restful api server with **gorilla/mux**.  
For simplifying code, this api uses a mock database that is `map[int]Object`

## Install and Run
```shell
$ go get -u github.com/gorilla/mux
$ go get github.com/google/uuid
$ go get github.com/stretchr/testify/assert
$ go get github.com/stretchr/objx
$ go get bitbucket.org/roisin087/fitnessclassbookings

$ cd $GOPATH/src/bitbucket.org/roisin087/fitnessclassbookings
$ go build
$ ./fitnessclassbookings
```

## API Endpoint
- http://localhost:3000/api/v1/classes
    - `GET`: get list of classes
    - `POST`: create class
- http://localhost:3000/api/v1/bookings
    - `GET`: get list of bookings
    - `POST`: create booking

## Data Structure
Booking
```json
{ 
   "id":1,
   "class_id":1,
   "member_id":"aigsf6sfasf",
   "studio_id":"f786af",
   "special_requirements":"none",
   "is_cancelled":false,
   "payment_reference":"sfas98a7sfasf",
   "meta_data":"",
   "creation_timestamp":"2020:09:02 14:56:23"
}
```

Class
```json
{ 
   "id":1,
   "name":"Class1",
   "description":"Pilates",
   "club":"over 40's",
   "start_date":"09022020",
   "end_date":"09032020",
   "duration_in_minutes":60,
   "capacity":20,
   "studio":"Tralee Pilates",
   "is_active":true,
   "creation_timestamp":"2020:09:02 14:56:23"
}
```