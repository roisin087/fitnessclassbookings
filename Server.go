package main

import (
	"github.com/gorilla/mux"
	"fitnessclassbookings/v1/handler"
	"log"
	"net/http"
)

func main() {
	router := mux.NewRouter().StrictSlash(true)
	sub := router.PathPrefix("/api/v1").Subrouter()
	sub.Methods("GET").Path("/bookings").HandlerFunc(v1.GetBookings)
	sub.Methods("POST").Path("/bookings").HandlerFunc(v1.SubmitBooking)
	sub.Methods("GET").Path("/classes").HandlerFunc(v1.GetClasses)
	sub.Methods("POST").Path("/classes").HandlerFunc(v1.SubmitClass)

	log.Fatal(http.ListenAndServe(":3000", router))
}
