package v1

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	//"github.com/gorilla/mux"
	"fitnessclassbookings/domain/domainmodels"
	"fitnessclassbookings/domain/mapping"
	repository "fitnessclassbookings/data/repository"
	classWorkflow "fitnessclassbookings/domain/workflows/class"
)


func GetClasses(responseWriter http.ResponseWriter, _ *http.Request) {
	classes := repository.GetClasses()
	mappedClasses := make([]domainmodels.Class, 0, len(classes))
	for _, class := range classes {
		mappedClass := mapping.MapClassToDomain(class)
		mappedClasses = append(mappedClasses, mappedClass)
	}

	if(len(classes) == 0){
		http.Error(responseWriter, "No Classes found", http.StatusNoContent)
	}
	responseWriter.WriteHeader(200)
	responseWriter.Header().Set("Content-Type", "application/json; charset=UTF-8")
	json.NewEncoder(responseWriter).Encode(classes) 	
}


// TODO ROC: idempotent check
func SubmitClass(responseWriter http.ResponseWriter, request *http.Request) {
	requestBody, err := ioutil.ReadAll(request.Body)
	if err != nil {
		http.Error(responseWriter, err.Error(), http.StatusInternalServerError)
	}

	class := new(domainmodels.Class)
	err = json.Unmarshal(requestBody, class)
	if err != nil {
		http.Error(responseWriter, err.Error(), http.StatusBadRequest)
	}

	savedClass, responseStatusCode := classWorkflow.SubmitClassExecute(*class)
	if(responseStatusCode == http.StatusCreated){
		responseWriter.Header().Set("Content-Type", "application/json; charset=UTF-8")
		responseWriter.WriteHeader(http.StatusCreated)
		json.NewEncoder(responseWriter).Encode(savedClass) 
	}
}