package v1

import (
	"encoding/json"
	"fitnessclassbookings/domain/domainmodels"
	"fitnessclassbookings/domain/mapping"
	repository "fitnessclassbookings/data/repository"
	bookingWorkflow "fitnessclassbookings/domain/workflows/booking"
	"io/ioutil"
	"net/http"
	"log"
)


func GetBookings(responseWriter http.ResponseWriter, _ *http.Request) {
	bookings := repository.GetBookings()
	mappedBookings := make([]domainmodels.Booking, 0, len(bookings))
	for _, booking := range bookings {
		mappedBooking := mapping.MapBookingToDomain(booking)
		mappedBookings = append(mappedBookings, mappedBooking)
	}

	if(len(mappedBookings) == 0){
		http.Error(responseWriter, "No Bookings found", http.StatusNoContent)
	}
	responseWriter.WriteHeader(200)
	responseWriter.Header().Set("Content-Type", "application/json; charset=UTF-8")
	json.NewEncoder(responseWriter).Encode(mappedBookings) 	
}


func SubmitBooking(responseWriter http.ResponseWriter, request *http.Request) {
	//TODO ROC: Setup proper logging
	requestBody, err := ioutil.ReadAll(request.Body)
	if err != nil {
		log.Print(err)
		http.Error(responseWriter, err.Error(), http.StatusInternalServerError)
	}

	booking := new(domainmodels.Booking)
	err = json.Unmarshal(requestBody, booking)
	if err != nil {
		invalidRequestError := NewError(http.StatusBadRequest, "invalid request", err)
		log.Print(invalidRequestError)
		invalidRequestErrorAsJson, jsonError := json.Marshal(invalidRequestError)
		if(jsonError == nil){
			http.Error(responseWriter, string(invalidRequestErrorAsJson), http.StatusBadRequest)
		}
		http.Error(responseWriter, err.Error(), http.StatusBadRequest)
	}

	bookingResponse, responseStatusCode := bookingWorkflow.SubmitBookingExecute(*booking);

	if(responseStatusCode == http.StatusCreated){
		responseWriter.WriteHeader(http.StatusCreated)
		responseWriter.Header().Set("Content-Type", "application/json; charset=UTF-8")
		json.NewEncoder(responseWriter).Encode(bookingResponse) 
	}

	if(responseStatusCode == http.StatusInternalServerError){
		http.Error(responseWriter, err.Error(), http.StatusInternalServerError)
	}
}