package v1

type Error struct {
	Code int
	Type string
    ErrorMessage error
}


func NewError(code int, errorType string, message error) *Error {
    return &Error{
		Code: code,
		Type: errorType,
        ErrorMessage: message }
}


func (err *Error) Error() (int, string, error) {
    return err.Code, err.Type, err.ErrorMessage
}