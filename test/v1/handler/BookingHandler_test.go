package test

import(
	"testing"
	"net/http/httptest"
	"net/http"
	"strings"
	"fmt"
	"fitnessclassbookings/v1/handler"
	"github.com/stretchr/testify/assert"
	//"github.com/stretchr/testify/mock"
)

func TestGetBookings_Success(test *testing.T) {
	request, err := http.NewRequest("GET", "api/v1/bookings", nil)
	if err != nil {
		test.Fatal(err)
	}
	response := httptest.NewRecorder()
	
	handler := http.HandlerFunc(v1.GetBookings)
	handler.ServeHTTP(response, request)

	if assert.NotNil(test, response) {
		assert.Equal(test, response.Code, 200)
	}
}

func TestSubmitBooking_Success(test *testing.T) {
	bookingRequest := fmt.Sprintf(`{"id":2, "class_id":1,"member_id":"aigsf6sfasf","studio_id":"f786af","special_requirements":"faasf98af","is_cancelled":true,"payment_reference":"sfas98a7sfasf","meta_data":"","creation_timestamp":"2020:09:02 14:56:23"}`)
	request, err := http.NewRequest("POST", "api/v1/bookings", strings.NewReader(bookingRequest))
	if err != nil {
		test.Fatal(err)
	}
	response := httptest.NewRecorder()
	
	handler := http.HandlerFunc(v1.SubmitBooking)
	handler.ServeHTTP(response, request)

	expected := `{"id":2,"booking_reference":"a7103e59-4d1f-11ea-8a2d-7446a03cfd1d","member_id":"aigsf6sfasf","class_id":1,"class_date":"","studio_id":"f786af","special_requirements":"faasf98af","is_cancelled":true,"payment_reference":"sfas98a7sfasf","meta_data":"","creation_timestamp":"2020-02-11 22:41:33.0446252 +0000 UTC"}`
	if assert.NotNil(test, response) {
		assert.Equal(test, response.Code, 201)
		// TODO ROC: Deserialize body to assert on all fields except booking_reference (uuid)
		assert.Equal(test, response.Body.String(), expected)
	}
}

func TestGetBookings_Success_NoContent(test *testing.T) {
	request, err := http.NewRequest("GET", "api/v1/bookings", nil)
	if err != nil {
		test.Fatal(err)
	}
	response := httptest.NewRecorder()
	
	handler := http.HandlerFunc(v1.GetBookings)
	handler.ServeHTTP(response, request)

	if assert.NotNil(test, response) {
		assert.Equal(test, response.Code, http.StatusNoContent)
	}
}