package booking

import(
	"fitnessclassbookings/domain/domainmodels"
	"fitnessclassbookings/domain/services"
	"fitnessclassbookings/domain/mapping"
	"net/http"
)


func SubmitBookingExecute(booking domainmodels.Booking) (*domainmodels.Booking, int) {	
	confirmedBooking, err := services.Book(booking)
	mappedBooking := mapping.MapBookingToDomain(confirmedBooking)

	if(err != nil){
		return &mappedBooking, http.StatusInternalServerError
	}

	return &mappedBooking, http.StatusCreated
}