package class

import(
	"fitnessclassbookings/domain/domainmodels"
	classRepository "fitnessclassbookings/data/repository"
	"fitnessclassbookings/domain/mapping"
	"net/http"
)


func SubmitClassExecute(class domainmodels.Class) (*domainmodels.Class, int) {	
	mappedDataClass := mapping.MapClassToData(class)
	classRepository.SaveClass(mappedDataClass)
	mappedDomainClass := mapping.MapClassToDomain(mappedDataClass)
	
	return &mappedDomainClass, http.StatusCreated
}