package services

import(
	"fitnessclassbookings/domain/domainmodels"
	"fitnessclassbookings/domain/mapping"
	"fitnessclassbookings/data/datamodels"
	repository "fitnessclassbookings/data/repository"
)


func Book(booking domainmodels.Booking) (datamodels.Booking, error){
	mappedBooking, mappedBookingError := mapping.MapBookingToData(booking)
	if(mappedBookingError == nil){
		repository.SaveBooking(mappedBooking)
		return mappedBooking, nil
	}
	
	return mappedBooking, mappedBookingError
}