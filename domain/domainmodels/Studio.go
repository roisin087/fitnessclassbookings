package domainmodels


type Studio struct {
	Id string `json:"id"`
	Name string `json:"name"`
	Address string `json:"address"`
	Phone string `json:"phone"`
	Email string `json:"email"`
	WebsiteURL string `json:"website_url"`
	IsActive bool `json:"is_active"`
	CreationTimestamp string `json:"creation_timestamp"`
}