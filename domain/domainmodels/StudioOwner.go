package domainmodels


type StudioOwner struct {
	Id string `json:"id"`
	Name string `json:"name"`
	Address string `json:"address"`
	HomePhone string `json:"home_phone"`
    Mobile string `json:"mobile"`
	Email string `json:"email"`
	Studios []Studio `json:"studios"`
	IsActive bool `json:"is_active"`
	CreationTimestamp string `json:"creation_timestamp"`
}