package domainmodels


type Class struct {
	Id int `json:"id"`
	Name string `json:"name"`
	Description string `json:"description"`
	Club string `json:"club"`
	StartDate string `json:"start_date"`
	EndDate string `json:"end_date"`
	DurationInMinutes int `json:"duration_in_minutes"`
	Capcity int`json:"capacity"`
	StudioId string `json:"studio"`
	IsActive bool `json:"is_active"`
	CreationTimestamp string `json:"creation_timestamp"`
}
