package domainmodels


type Booking struct {
	Id int `json:"id"` // would be a uuid if using a database
	BookingReference string `json:"booking_reference"` 
	MemberId string `json:"member_id"`
	ClassId int `json:"class_id"`
	ClassDate string `json:"class_date"`
	StudioId string `json:"studio_id"`
	SpecialRequirements string `json:"special_requirements"`
	IsCancelled bool `json:"is_cancelled"`
	PaymentReference string `json:"payment_reference"`
	MetaData string `json:"meta_data"`
	CreationTimestamp string `json:"creation_timestamp"`
}