package mapping

import(
	"fitnessclassbookings/domain/domainmodels"
	"fitnessclassbookings/data/datamodels"
	"github.com/google/uuid"
	"time"
)

func MapBookingToData(booking domainmodels.Booking) (datamodels.Booking, error){
	guid, err := uuid.NewUUID()

	mappedBooking := datamodels.Booking{
		Id: booking.Id,
		BookingReference: guid.String(),
		MemberId: booking.MemberId,
		ClassId: booking.ClassId,
		ClassDate: booking.ClassDate,
		StudioId: booking.StudioId,
		SpecialRequirements: booking.SpecialRequirements,
		IsCancelled: booking.IsCancelled,
		PaymentReference: booking.PaymentReference,
		MetaData: booking.MetaData,
		ActionTimestamp: booking.CreationTimestamp,
		CreationTimestamp: time.Now().UTC().String(),
	}
	return mappedBooking, err
}


func MapBookingToDomain(booking datamodels.Booking) (domainmodels.Booking){
	mappedBooking := domainmodels.Booking{
		Id: booking.Id,
		BookingReference: booking.BookingReference,
		MemberId: booking.MemberId,
		ClassId: booking.ClassId,
		ClassDate: booking.ClassDate,
		StudioId: booking.StudioId,
		SpecialRequirements: booking.SpecialRequirements,
		IsCancelled: booking.IsCancelled,
		PaymentReference: booking.PaymentReference,
		MetaData: booking.MetaData,
		CreationTimestamp: booking.CreationTimestamp,
	}
	return mappedBooking
}


func MapClassToData(class domainmodels.Class) (datamodels.Class){
	mappedClass := datamodels.Class{
		Id: class.Id,
		Name: class.Name,
		Description: class.Description,
		Club: class.Club,
		StartDate: class.StartDate,
		EndDate: class.EndDate,
		DurationInMinutes: class.DurationInMinutes,
		Capcity: class.Capcity,
		StudioId: class.StudioId,
		IsActive: class.IsActive,
		CreationTimestamp: time.Now().UTC().String()}

	return mappedClass
}


func MapClassToDomain(class datamodels.Class) (domainmodels.Class){
	mappedClass := domainmodels.Class{
		Id: class.Id,
		Name: class.Name,
		Description: class.Description,
		Club: class.Club,
		StartDate: class.StartDate,
		EndDate: class.EndDate,
		DurationInMinutes: class.DurationInMinutes,
		Capcity: class.Capcity,
		StudioId: class.StudioId,
		IsActive: class.IsActive,
		CreationTimestamp: time.Now().UTC().String()}

	return mappedClass
}
