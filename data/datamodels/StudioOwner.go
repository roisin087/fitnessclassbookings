package datamodels


type StudioOwner struct {
	Id string
	Name string
	Address string
	HomePhone string
    Mobile string
	Email string
	Studios []Studio
	IsActive bool
	CreationTimestamp string
}