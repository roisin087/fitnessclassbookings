package datamodels


type Studio struct {
	Id string
	Name string
	Address string
	Phone string
	Email string
	WebsiteURL string
	IsActive bool
	CreationTimestamp string
}