package datamodels


type Booking struct {
	Id int // would be a uuid if using a database
	BookingReference string
	MemberId string
	ClassId int
	ClassDate string
	StudioId string
	SpecialRequirements string
	IsCancelled bool
	PaymentReference string
	MetaData string
	ActionTimestamp string
	CreationTimestamp string

	// TODO ROC: validate fields and return custom Error struct with code 400 BadRequest and invalid field/s
}