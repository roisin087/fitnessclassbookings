package datamodels


type Class struct {
	Id int
	Name string
	Description string
	Club string
	StartDate string
	EndDate string
	DurationInMinutes int
	Capcity int
	StudioId string // uuid
	IsActive bool
	CreationTimestamp string

	// TODO ROC: validate fields
}
