package data

import (
	"fitnessclassbookings/infrastructure/database"
	"fitnessclassbookings/data/datamodels"
)


func GetBookings() ([]datamodels.Booking) {
	return database.FindAllBookings()
}


func GetBooking(bookingId int) (datamodels.Booking, bool) {
	booking, ok := database.FindBookingById(bookingId)
	return booking, ok
}


func SaveBooking(booking datamodels.Booking) {
	database.SaveBooking(booking.Id, booking)
}