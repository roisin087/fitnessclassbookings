package data

import (
	"fitnessclassbookings/infrastructure/database"
	"fitnessclassbookings/data/datamodels"
)


func GetClasses() ([]datamodels.Class) {
	return database.FindAllClasses()
}


func SaveClass(class datamodels.Class) {
	database.SaveClass(class.Id, class)
}